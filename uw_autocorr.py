import sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal
from puwr import tauint
plt.style.use("standard.mplstyle")

"""
Kimmy Cushman - understandable version of UW code at https://github.com/dhesse/py-uwerr/blob/master/
still need to cite paper [1]
[1] U. Wolff [**ALPHA** Collaboration], *Monte Carlo errors with less
   errors*, Comput. Phys. Commun.  **156**, 143 (2004)
   ``[hep-lat/0306017]``

Basic goal is to calculate the integrated autocorrelation time to correct errors on data
Generalized to give corrected error bars of functions of the data 
y = f(x)
report y +/- sigma 
sigma = sigma_naive * sqrt(2 * tau_int)
with sigma_naive given by boot error bars OR taylor expansion of f around means as in Eq 16 of [1] 
beware of factor of 1/sqrt{N} in final result 

Calculate autocorrelation function Gamma(Delta)
Calculate tau_int(W) = C_F(W)/ Gamma_F(0)  Eqations 35, 34 of [1] (C_F is sum of Gamma(Delta) up to Delta = W) 
Find optimal window W by minimizing sum of theoretical systematic and statistical error Eq 43 of [1] by 
setting derivative = 0 (Equation 52 of [1])

Note that this code follows the paper, and computes the projected autocorrelation "Gtil" given by Equation 33 
by using variables of Equation 37 of [1] instead of computing every element of Gamma_{alpha beta}. 
To use this functionality, specify a function in main function of the form
def function(*args):
    # first N_alpha arguments must be observables of data
    # return must be scalar
    ...
and use f = function. 
But the main function here takes an integer to study autocorrelation of C(t) for specific t, i.e. f = int(argv[1]) 
"""


def derivative(f, alpha, h):
    """
    Equation 39 of [1]
    :param f: function of data[alpha] (ex. effective mass at specific timeslice)
    or int (see project_derivative function)
    :param alpha: vector or ints
    :param h: float
    :return: derivative of f according to Eq. 39 of [1]
    """
    def df(*a):
        args = list(a)
        args_plus_h = args.copy()
        args_plus_h[alpha] += h
        args_minus_h = args.copy()
        args_minus_h[alpha] -= h
        f_high = f(*args_plus_h)
        f_low = f(*args_minus_h)
        return (f_high - f_low)/h
    return df


def project_derivative(f, data, means, N, R, N_alpha):
    """
    calculate derivative and evaluate at means, then project (Eq 37 of [1])
    :param f: function of data[alpha] (ex. effective mass at specific timeslice)
    or int
    :param data: data[alpha][r][i] where alpha is observable, for example C(t=14),
    r is Monte Carlo stream, i is index of Monte Carlo samples
    :param means: np.mean(np.mean(data, axis=1), axis=1), AKA mean of data over r and i indices
    :param N: int, total number of samples
    :param R: int, number of Monte Carlo streams
    :param N_alpha: int, number of observables measured (usually Nt)
    :return: a_f^{i,r} of Eq. 37 of [1]
    """
    if type(f) == int:
        return data[f]

    Nr = int(N/R)  # number of samples per stream
    # calculate h from variance of data as in Equation 38 of [1]
    # first calculate diagonal variance, as Equation 31 of [1], with alpha=beta and t=0
    G = np.zeros(N_alpha)
    for alpha in range(N_alpha):
        for r in range(R):
            for i in range(Nr):
                G[alpha] += (data[alpha, r, i] - means[alpha])**2/N
    # calculate h according to Equation 38 of [1]
    h = np.array([np.sqrt(G_alpha/N) for G_alpha in G])

    # calculate derivative of chosen f, evaluate at means, and project out alpha index
    df = []
    for alpha in range(N_alpha):
        h_alpha = h[alpha]
        df.append(derivative(f, alpha, h_alpha))

    # evaluate derivative at mean
    fa = [df_alpha(*means) for df_alpha in df]

    # project a[alpha,r,i] -> a_f[r,i]  # Equation 37 of [1]
    a_f = np.zeros((R, Nr))
    for r in range(R):
        for i in range(Nr):
            for alpha in range(N_alpha):
                a_f[r, i] += fa[alpha] * data[alpha, r, i]
    return a_f


def gamma(a_f, R, N):
    """
    Calculate autocorrelation function as in Eq. 31 of [1], but instead of a_alpha^{i,r} (aka the data)
    use projected data a_f^{i,r} for specific function f, with
    :param a_f: array of floats a_f[r][i] calculated in project function (Eq. 37 or [1])
    :param R: int, number of Monte Carlo streams
    :param N: int, total number of samples
    :return:    Gtil_by_hand: projected autocorrelation function,
                mean_f: float, projected mean of a_f (average over r,i indices)
    """
    Nr = int(N/R)  # number of samples per stream

    # a_f[r, i]
    # average of df/dA_alpha * a_alpha
    mean_f = np.mean(np.mean(a_f, axis=0), axis=0)  # \bar{\bar{a}} as in Equation 31 of [1], but projected
    # Equation 31 of [1] with t = 0,...,delta_max-1
    delta_max = Nr
    Gtil_by_hand = np.zeros(delta_max)
    for r in range(R):
        for delta in range(delta_max):
            for i in range(Nr - delta):
                Gtil_by_hand[delta] += (a_f[r, i] - mean_f)*(a_f[r, i+delta] - mean_f)/(N - delta*R)

    # # # an explicit version of UW code, faster for large data sets, gives same result as above
    # Gtil = np.zeros(Nr)
    # for r in range(R):
    #     f_t = np.zeros(Nr)
    #     g_t = np.zeros(Nr)
    #     for i in range(Nr):
    #         f_t[i] = a_f[r, i] - mean_f
    #         g_t[i] = a_f[r, Nr-1 - i] - mean_f
    #     term = scipy.signal.fftconvolve(f_t, g_t, mode='full')[Nr-1::-1]
    #     Gtil += term
    # Gtil = Gtil * np.array([1/(N - R*delta) for delta in range(len(Gtil))])
    # diff = (Gtil_by_hand - Gtil[:delta_max])/Gtil[:delta_max]  # == 0
    # print(diff)
    return Gtil_by_hand, mean_f


def tau_int(a_f, R, N):
    """
    Calculate optimal W to find optimal tau_int(W) = C_F(W)/v_F (Equations 41, 35, 34 of [1])
    according to minimizing cost function of Equation 43 of [1]
    :param a_f: a_f^{i,r} of Eq. 37 of [1]
    :param R: int, number of Monte Carlo streams
    :param N: total number of samples N = R * Nr,
    :return:    tau_opt: integrated autocorrelation time to be quoted
                W_opt: optimal W of Equation 35 of [1] according to minimizing cost
                tint: integrated autocorrelation time as in Equation 41 of [1]
                Gtil: projected autocorrelation time as in Equation 33 of [1]
    """
    Gtil, mean_f = gamma(a_f, R, N)
    W_max = len(Gtil)
    # tau_int = G(0) + sum_{delta = 1}^W Gamma(delta)
    # W = 0 is not defined

    CF_W = np.zeros(W_max)  # Equation 35 from [1]
    tint = np.zeros(W_max)
    for W in range(W_max):
        # sum gives 0 for sum(Gtil[1:0]) and sum(Gtil[1:1])
        # need W + 1 since Equation 35 is sum_{t=1}^W inclusive
        CF_W[W] = Gtil[0] + 2*sum(Gtil[1:W+1])
        tint[W] = CF_W[W]/(2 * Gtil[0])  # Equation 41 of [1]
    # suppress errors from overflow
    # der_cost = -tau x derivative of cost function defined in Equation 43 of [1],
    # der_cost = Equation 52 of [1]
    W_opt = 0
    np.seterr(over='ignore')
    for W in range(W_max):  # invalid for W=0 since 1/sqrt(W)
        if W == 0:
            der_cost = 1  # placeholder
        else:
            der_cost = np.exp(-W/tint[W]) - tint[W]/np.sqrt(W*N)
        # optimal W where der_cost changes sign from positive to negative (estimates deriv=0)
        if der_cost < 0:
            W_opt = W
            break
    # end suppress
    np.seterr(over='warn')

    tau_opt = tint[W_opt]
    dtau_opt = tau_opt * 2 * np.sqrt((W_opt - tau_opt + 0.5) / N)  # error bar according to Equation 42 of [1]
    print(f"optimal tau_int = {tau_opt} +/- {dtau_opt}, W = {W_opt}")

    sigma_naive = np.sqrt(Gtil[0]/N)
    sigma_corrected = sigma_naive * np.sqrt(2*tau_opt)
    print("sigma naive = {:.3e}, sigma corrected = {:.3e}".format(sigma_naive, sigma_corrected))
    print()

    cost = calculate_cost(W_max, tau_opt, N)
    return tau_opt, W_opt, tint, Gtil, cost


def calculate_cost(W_max, tau_opt, N):
    # 2 x Equation 43 of [1]
    cost = np.zeros(W_max)
    for W in range(W_max):
        if W == 0:
            cost[W] = 1
        else:
            cost[W] = np.exp(-W/tau_opt) + 2 * np.sqrt(W/N)
    return cost


def plot_tau_G_cost(tint, Gtil, cost, W_opt, N, xmax):
    """
    plot tau int and autocorrelation as a function of W
    :param tint: integrated autocorrelation time as in Equation 41 of [1]
    :param Gtil: projected autocorrelation time as in Equation 33 of [1]
    :param W_opt: optimal W of Equation 35 of [1] according to minimizing cost
    :param N: int, total number of samples N = R * Nr, where Nr is number in each stream
    :param xmax: max W value on x-axis to show on plot
    :return: None
    """
    # error bar on tau_int according to Eq. 42 of [1]
    max_Delta = len(Gtil)
    dtint = np.zeros(max_Delta)
    for W in range(max_Delta):
        if W == 0:
            dtint[W] = 1
        else:
            dtint[W] = tint[W] * 2 * np.sqrt((W - tint[W] + 0.5) / N)

    # start with W=1
    x = np.arange(1, xmax)
    fig, axes = plt.subplots(3, figsize=(10, 7), sharex=True)

    # tau int vs W
    y = tint[1:xmax]
    yerr = dtint[1:xmax]
    x_best = [W_opt]
    y_best = [tint[W_opt]]
    y_err_best = [dtint[W_opt]]
    axes[0].set_ylabel(r'$\tau_{\mathrm{int}}$')
    axes[0].set_xlabel(r'$W$')
    axes[0].errorbar(x, y, yerr, fmt="o", color='b')
    axes[0].errorbar(x_best, y_best, y_err_best, fmt='o', color='r')

    # cost(W) vs W
    y = cost[1:xmax]
    x_best = [W_opt]
    y_best = [cost[W_opt]]
    axes[1].set_ylabel(r'$\delta(\sigma_F)/\sigma_F$')
    axes[1].set_xlabel(r'$W$')
    axes[1].plot(x, y, marker="o", color='b', markersize=8)
    axes[1].plot(x_best, y_best, marker='o', color='r', markersize=8)

    # G(Delta) vs Delta
    y = Gtil[1:xmax]
    x_best = [W_opt]
    y_best = [Gtil[W_opt]]
    axes[2].set_ylabel(r'$\Gamma(\Delta)$')
    axes[2].set_xlabel(r'$\Delta$')
    axes[2].axhline(0, color='k')
    axes[2].plot(x, y, marker="o", color='b', markersize=8)
    axes[2].plot(x_best, y_best, marker='o', color='r', markersize=8)

    plt.tight_layout()
    #plt.savefig("L32T64_beta11p028_kcfgQUENCHED_k0p1554_Buchoff_POINT_SOURCE_POINT_SINK_pion_N180_autocorr_t16.pdf")
    plt.savefig("independent_autocorr_analysis.pdf")
    plt.show()


def main():
    t_study = int(sys.argv[1])
    f = t_study
    Lx = 32
    # Nt = 2*Lx
    Nt = 1
    N = 180  # total number of samples
    R = 1  # number of streams
    N_alpha = Nt  # to follow notation from [1], alpha is index of observable, here used as data[alpha] = C(t=alpha)

    # load data into numpy array with data[alpha][r][i] as paragraph before Equation 5 of [1].
    # Here alpha represents timeslices
    #data_raw = np.load("Data/L32T64_beta11p028_kcfgQUENCHED_k0p1554_Buchoff_POINT_SOURCE_POINT_SINK_pion_N180.npy")
    data_raw = np.load("independently_sampled.npy")
    data = np.zeros((Nt, R, N))
    Nr = int(N/R)
    for t in range(Nt):
        for r in range(R):
            for i in range(Nr):
                #data[t, r, i] = np.real(data_raw[i, t])
                data[t, r, i] = np.real(data_raw[i])

    # mean of data over i,r indices as in Equation 7 of [1]
    means = np.zeros(Nt)
    for alpha in range(N_alpha):
        for r in range(R):
            means[alpha] += Nr * np.mean(data[alpha, r])
    means = means/N

    # calculate derivative and evaluate at means, then project (Eq 37 of [1])
    a_f = project_derivative(f, data, means, N, R, N_alpha)

    # get optimal tau int, optimal window, and tau int, autocorrelation (of function) and cost as a function of W
    # print optimal tau_int with errors to terminal
    tau_opt, W_opt, tint, Gtil, cost = tau_int(a_f, R, N)

    # plot tau int, autocorrelation (of function) and cost as a function of W,
    plot_tau_G_cost(tint, Gtil, cost, W_opt, N, xmax=int(N/4))

    # get equivalent output from UW code
    mean, delta, tint, d_tint = tauint(data, f)
    print("Ulli Wolff code output:")
    print("tau_int = {0} +/- {1}".format(tint, d_tint))
    print("mean = {0} +/- {1}".format(mean, delta))


if __name__ == "__main__":
    main()
